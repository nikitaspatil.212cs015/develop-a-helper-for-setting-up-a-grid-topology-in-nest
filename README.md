# Develop a helper for setting up a grid topology in NeST

CS751 Course Assignment - Develop a helper for setting up a grid topology in NeST

## Overview

Grid topology is one of the popular topologies for studying network performance. In a regular grid topology, each node in the network is connected with two neighbors along one or more dimensions. The topology is basically a grid structure of nodes. The senders are the top-left side nodes and receivers are the bottom-right side nodes. In-between are routers. Creating such a topology is very time consuming when the number of nodes are too many like 10, 20, 30 or 100. So, setting up such a topology manually every time is very hectic process in simulators/emulators. Therefore, this project aims to develop a helper for setting up grid topology in NeST by providing APIs to users.

The helper will take many parameters like number of rows, number of columns in X x Y pattern, the network IP address, subnet mask, TCP/UDP flow, and some more as per the requirements of user. Then this helper will assign IP addresses to all nodes automatically to all the interfaces. We will have a base class named topology-helper and our grid-helper class will inherit from that.
